import json


class FFprobeDecoder(json.JSONDecoder):

    def decode(self, json_str):
        result = super().decode(json_str)
        return self._decode(result)

    def _decode(self, obj):
        if isinstance(obj, str):
            for cast in (int, float):
                try:
                    return cast(obj)
                except ValueError:
                    continue
            return obj
        elif isinstance(obj, dict):
            return {k: self._decode(v) for k, v in obj.items()}
        elif isinstance(obj, list):
            return [self._decode(v) for v in obj]
        else:
            return obj
