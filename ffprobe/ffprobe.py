from os import path, listdir
from subprocess import check_call, check_output
import json
import logging
from types import SimpleNamespace

from ffprobe.utils import FFprobeDecoder

FFMPEG = 'ffmpeg'
FFPROBE = 'ffprobe'

FFMPEG_BASE_COMMAND = [FFMPEG, '-hide_banner']
FFPROBE_BASE_COMMAND = [FFPROBE, '-hide_banner']
FFPROBE_JSON = FFPROBE_BASE_COMMAND + [
    '-loglevel', 'quiet',
    '-print_format', 'json', '-show_format', '-show_streams'
]


class FFprobe:

    def __init__(self, filename):
        medium_dict = self._ffprobe_medium(filename)

        self.streams = [Stream(**s_dict) for s_dict in medium_dict['streams']]
        self.format = Format(**medium_dict['format'])

    def _ffprobe_medium(self, medium_filename):
        cmd = list(FFPROBE_JSON)
        cmd += [medium_filename]

        medium_info_json = check_output(cmd)
        medium_info = json.loads(medium_info_json, cls=FFprobeDecoder)

        return medium_info


class Stream(SimpleNamespace):
    pass


class Format(SimpleNamespace):
    pass
